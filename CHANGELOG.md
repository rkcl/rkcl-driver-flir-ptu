# [](https://gite.lirmm.fr/rkcl/rkcl-driver-flir-ptu/compare/v1.1.0...v) (2021-09-30)


### Bug Fixes

* call init before start in app ([6c25eaa](https://gite.lirmm.fr/rkcl/rkcl-driver-flir-ptu/commits/6c25eaabc275d4c35bf2ab0614a200e25ecc8299))
* incorrect use of joint group ([aa9c5e3](https://gite.lirmm.fr/rkcl/rkcl-driver-flir-ptu/commits/aa9c5e3e8004bb2e41adcf5fd0b908d579ca047b))


### Features

* update dep to flir ptu sdk ([1d18b0d](https://gite.lirmm.fr/rkcl/rkcl-driver-flir-ptu/commits/1d18b0df2b79558a4e42b0c0fcbcdad1b1cba743))
* use conventional commits ([357104d](https://gite.lirmm.fr/rkcl/rkcl-driver-flir-ptu/commits/357104d2c53e5a6186b0df6691175e5a54307c70))



# [1.1.0](https://gite.lirmm.fr/rkcl/rkcl-driver-flir-ptu/compare/v1.0.0...v1.1.0) (2020-11-27)



# [1.0.0](https://gite.lirmm.fr/rkcl/rkcl-driver-flir-ptu/compare/v0.2.0...v1.0.0) (2020-03-12)



# 0.2.0 (2020-02-20)



