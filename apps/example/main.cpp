/**
 * @file main.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief
 * @date 12-03-2020
 * License: CeCILL
 */

#include <rkcl/drivers/flir_ptu_driver.h>
#include <rkcl/processors/simple_joints_controller.h>
#include <yaml-cpp/yaml.h>

#include <iostream>
#include <thread>
#include <chrono>

int main()
{
    auto joint_group = std::make_shared<rkcl::JointGroup>();
    joint_group->resize(2);

    std::cout << "Creating driver for Flir PTU..." << std::endl;
    auto driver = rkcl::FlirPanTiltDriver("192.168.0.101", joint_group);
    std::cout << "Driver created !" << std::endl;

    std::cout << "Creating controller for Flir PTU..." << std::endl;
    auto joint_controller = rkcl::SimpleJointsController(joint_group);
    std::cout << "Controller created !" << std::endl;

    std::cout << "Initializing driver ..." << std::endl;
    if (not driver.init(0.))
    {
        std::cerr << "Unable to init" << std::endl;
        std::exit(-1);
    }
    std::cout << "Driver initialized !" << std::endl;

    std::cout << "Starting driver ..." << std::endl;
    if (not driver.start())
    {
        std::cerr << "Unable to start" << std::endl;
        std::exit(-1);
    }
    std::cout << "Driver started !" << std::endl;

    driver.sync();
    if (not driver.read())
    {
        std::cerr << "Unable to read" << std::endl;
        std::exit(-1);
    }

    std::cout << joint_group->state().position().transpose() << std::endl;

    joint_group->target().velocity()(0) = -1; // pan
    joint_group->target().velocity()(1) = 0;  // tilt

    joint_controller();

    if (not driver.send())
    {
        std::cerr << "Unable to send" << std::endl;
        std::exit(-1);
    }

    std::this_thread::sleep_for(std::chrono::seconds(1));

    joint_group->target().velocity()(0) = 0; // pan
    joint_group->target().velocity()(1) = 0; // tilt

    joint_controller();

    if (not driver.send())
    {
        std::cerr << "Unable to send" << std::endl;
        std::exit(-1);
    }

    std::this_thread::sleep_for(std::chrono::seconds(1));

    if (not driver.stop())
    {
        std::cerr << "Unable to stop" << std::endl;
        std::exit(-1);
    }
}